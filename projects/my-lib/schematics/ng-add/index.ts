import { Rule, SchematicContext, Tree, branchAndMerge, SchematicsException } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { normalize } from 'path';
import * as ts from 'typescript';
import { addSymbolToNgModuleMetadata } from '../utility/ast-utils';
import { InsertChange } from '../utility/change';

// Just return the tree
export function ngAdd(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.addTask(new NodePackageInstallTask());
    console.log('options', _options);

    // Hardcoded path for the sake of simplicity
    const appModule = './src/app/app.module.ts';

    const rule = branchAndMerge(addDeclarationToAppModule(appModule));
    return rule(tree, _context);
    return tree;
  };
}
export function addDeclarationToAppModule(appModule: string): Rule {
  return (host: Tree) => {
    if (!appModule) {
      return host;
    }

    const modulePath = normalize(/*'/' +*/ appModule);
    //const modulePath = appModule;
    console.log('modulePath', modulePath);
    const text = host.read(modulePath);
    if (text === null) {
      throw new SchematicsException(`File ${modulePath} does not exist.`);
    }
    const sourceText = text.toString('utf-8');
    const source = ts.createSourceFile(
      modulePath,
      sourceText,
      ts.ScriptTarget.Latest,
      true
    );

    const changes = addSymbolToNgModuleMetadata(
      source,
      modulePath,
      'imports',
      'LoggerModule',
      '@my/logger-lib',
      'LoggerModule.forRoot({ enableDebug: true })'
    );

    const recorder = host.beginUpdate(modulePath);
    for (const change of changes) {
      if (change instanceof InsertChange) {
        recorder.insertLeft(change.pos, change.toAdd);
      }
    }
    host.commitUpdate(recorder);

    return host;
  };
}
